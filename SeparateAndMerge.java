import java.util.Scanner;
import java.util.*;

public class SeparateAndMerge {
   
   public static void main(String[] args) {
      
      LList<Integer> intList = new LList<Integer>();
      LList<Float> floatList = new LList<Float>();
      Scanner input = new Scanner(System.in);
      
      System.out.print("Please enter numbers: ");
      
      
      while(input.hasNext()) {
         if(input.hasNextInt()) {
            intList.add(input.nextInt());
         }
         else if(input.hasNextFloat()) {
            floatList.add(input.nextFloat());
         }
         else
            input.next();
      }
      
      System.out.print("Inputted values: ");
      
      Iterator<Integer> iterInt = intList.iterator();
      Iterator<Float> iterFloat = floatList.iterator();
      
      while(iterInt.hasNext() && iterFloat.hasNext()) {
         System.out.print(iterInt.next() + " ");
         System.out.print(iterFloat.next() + " ");
      }
      
      if(iterInt.hasNext()) {
         while(iterInt.hasNext())
            System.out.print(iterInt.next() + " ");
      }
      if(iterFloat.hasNext()) {
         while(iterFloat.hasNext())
            System.out.print(iterFloat.next() + " ");
      }   
   }
}
