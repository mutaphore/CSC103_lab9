import java.util.*;

public class LList <T> {
   
   private Node head;
   
   private class Node {
      
      T component;
      Node next;
   

      public Node(T data) {
         component = data;
         next = null;
      }
      
   }
   
   public void add(T item) {
      
      Node temp;
      Node walker;
      
      if(head == null) {
         head = new Node(item);
      }
      else {
         walker = head;
         
         while(walker.next != null)
            walker = walker.next;
         
         temp = new Node(item);
         walker.next = temp;
      }
      
   }
   
   public Iterator <T> iterator() { return new Iter();}
   
   private class Iter implements Iterator <T> {
      
      public Node cursor;
      
      public Iter() {
         cursor = head;
      }
      
      public boolean hasNext() {
         return cursor != null;
      }
      
      public T next() {
         
         if(!hasNext()) {
            throw new NoSuchElementException();
         }
         
         T answer = cursor.component;
         cursor = cursor.next;
         
         return answer;
      }
      
      
      
      public void remove() {
         throw new UnsupportedOperationException();
      }
   }
}
